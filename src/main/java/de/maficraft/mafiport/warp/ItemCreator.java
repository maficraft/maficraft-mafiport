package de.maficraft.mafiport.warp;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;


public class ItemCreator {
    private final ItemStack stack;
    private final ItemMeta meta;

    public ItemCreator(Material material, int amount, int subID) {
        this.stack = new ItemStack(material, amount, (short) subID);
        this.meta = this.stack.getItemMeta();
    }

    public ItemCreator setDisplayName(String displayName) {
        this.meta.setDisplayName(displayName);
        this.stack.setItemMeta(this.meta);
        return this;
    }

    public ItemCreator addLoreLine(String line) {
        ItemMeta meta = this.stack.getItemMeta();
        String[] split = line.split("//");
        ArrayList<String> lore = new ArrayList<>(Arrays.asList(split));
        meta.setLore(lore);
        this.stack.setItemMeta(meta);
        return this;
    }

    public ItemCreator addEnchantment(Enchantment enchantment, int power, boolean override) {
        this.meta.addEnchant(enchantment, power, override);
        this.stack.setItemMeta(this.meta);
        return this;
    }

    public ItemCreator setEnchanted(boolean enchanted) {
        if (enchanted) {
            addEnchantment(Enchantment.ARROW_DAMAGE, 1, true);
            this.meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        } else {
            for (Enchantment e : this.meta.getEnchants().keySet()) {
                this.meta.removeEnchant(e);
            }
        }
        this.stack.setItemMeta(this.meta);
        return this;
    }

    public ItemStack getStack() {
        return this.stack;
    }
}
