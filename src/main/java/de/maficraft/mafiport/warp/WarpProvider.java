package de.maficraft.mafiport.warp;

import de.maficraft.mafiport.MafiPortPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.Types;

public class WarpProvider {
    private final MafiPortPlugin plugin = MafiPortPlugin.getPlugin(MafiPortPlugin.class);
    private final String name;

    public WarpProvider(String name) {
        this.name = name;
    }

    public boolean exists() {
        return this.plugin.getMysqlConnection().hasEntries("SELECT warp FROM warp_list WHERE warp = ?", ps -> ps.setString(1, this.name));
    }

    public void create(Location location, int weight) {
        this.plugin.getMysqlConnection().prepare("INSERT INTO warp_list (warp,world,CordX,CordY,CordZ,CordYaw,CordPitch,weight) VALUES (?,?,?,?,?,?,?,?)", ps -> {
            ps.setString(1, this.name);
            ps.setString(2, location.getWorld().getName());
            ps.setDouble(3, location.getX());
            ps.setDouble(4, location.getY());
            ps.setDouble(5, location.getZ());
            ps.setFloat(6, location.getYaw());
            ps.setFloat(7, location.getPitch());
            ps.setInt(8, weight);
        });
    }

    public void create(Location location) {
        // create Warp ohne Schwierigkeitsgrad (weight)
        this.plugin.getMysqlConnection().prepare("INSERT INTO warp_list (warp,world,CordX,CordY,CordZ,CordYaw,CordPitch,weight) VALUES (?,?,?,?,?,?,?,?)", ps -> {
            ps.setString(1, this.name);
            ps.setString(2, location.getWorld().getName());
            ps.setDouble(3, location.getX());
            ps.setDouble(4, location.getY());
            ps.setDouble(5, location.getZ());
            ps.setFloat(6, location.getYaw());
            ps.setFloat(7, location.getPitch());
            ps.setNull(8, Types.INTEGER);
        });
    }

    public void remove() {
        this.plugin.getMysqlConnection().prepare("DELETE FROM warp_list WHERE warp = ?", ps -> ps.setString(1, this.name));
    }

    public Location getLocation() {
        String world = this.plugin.getMysqlConnection()
                .query("SELECT world FROM warp_list WHERE warp = ?",
                        ps -> ps.setString(1, this.name),
                        rs -> rs.getString("world"));
        double x = this.plugin.getMysqlConnection().query("SELECT CordX FROM warp_list WHERE warp = ?",
                ps -> ps.setString(1, this.name),
                rs -> rs.getDouble("CordX"));
        double y = this.plugin.getMysqlConnection().query("SELECT CordY FROM warp_list WHERE warp = ?",
                ps -> ps.setString(1, this.name),
                rs -> rs.getDouble("CordY"));
        double z = this.plugin.getMysqlConnection().query("SELECT CordZ FROM warp_list WHERE warp = ?",
                ps -> ps.setString(1, this.name),
                rs -> rs.getDouble("CordZ"));
        float yaw = this.plugin.getMysqlConnection().query("SELECT CordYaw FROM warp_list WHERE warp = ?",
                ps -> ps.setString(1, this.name),
                rs -> rs.getFloat("CordYaw"));
        float pitch = this.plugin.getMysqlConnection().query("SELECT CordPitch FROM warp_list WHERE warp = ?",
                ps -> ps.setString(1, this.name),
                rs -> rs.getFloat("CordPitch"));
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }
}
