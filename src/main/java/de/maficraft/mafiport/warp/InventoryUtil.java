package de.maficraft.mafiport.warp;

import de.ferdl9999.MafiWarp.Utils.WarpsManager;
import de.maficraft.mafijump.MafiJumpPlugin;
import de.maficraft.mafiport.MafiPortPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class InventoryUtil {
    private final MafiPortPlugin plugin = MafiPortPlugin.getPlugin(MafiPortPlugin.class);

    private final Map<UUID, Type> type = new HashMap<>();

    public Inventory warpInventory() {
        List<String> warps = new ArrayList<>();
        Inventory inventory = Bukkit.createInventory(null, 54, "§2Warps");

        putWarpsFromWeightInList(warps, 0);

        if (warps.size() > 45) {
            for (int i = 0; i < 45; i++) {
                inventory.setItem(i, (new ItemCreator(Material.EMERALD, 1, 0)).setDisplayName("§a" + warps.get(i))
                        .addLoreLine("§7Gewinn§8: §a" + WarpsManager.getReward(warps.get(i)) + " §7Mafis//§7Bestzeit§8: §a" +
                                getBest(warps.get(i)) + " §7Sekunden//§7Durchschnittszeit§8: §a" +
                                getAverage(warps.get(i)) + " §7Sekunden").getStack());
            }
            inventory.setItem(53, (new ItemCreator(Material.MAP, 1, 0)).setDisplayName("§22 §7Seite").getStack());
        } else {
            for (int i = 0; i < warps.size(); i++) {
                inventory.setItem(i, (new ItemCreator(Material.EMERALD, 1, 0)).setDisplayName("§a" + warps.get(i))
                        .addLoreLine("§7Gewinn§8: §a" + WarpsManager.getReward(warps.get(i)) + " §7Mafis//§7Bestzeit§8: §a" +
                                getBest(warps.get(i)) + " §7Sekunden//§7Durchschnittszeit§8: §a" +
                                getAverage(warps.get(i)) + " §7Sekunden").getStack());
            }
        }

        inventory.setItem(Type.EASY.getSlotID(), (new ItemCreator(Type.EASY.getSelection(), 1, 0)).setDisplayName("§7Sortieren: §aLeicht").setEnchanted(true).getStack());
        inventory.setItem(Type.NORMAL.getSlotID(), (new ItemCreator(Type.NORMAL.getSelection(), 1, 0)).setDisplayName("§7Sortieren: §eMittel").getStack());
        inventory.setItem(Type.HARD.getSlotID(), (new ItemCreator(Type.HARD.getSelection(), 1, 0)).setDisplayName("§7Sortieren: §cSchwer").getStack());
        inventory.setItem(Type.HARDCORE.getSlotID(), (new ItemCreator(Type.HARDCORE.getSelection(), 1, 0)).setDisplayName("§7Sortieren: §4Hardcore").getStack());
        inventory.setItem(Type.RANK.getSlotID(), (new ItemCreator(Type.RANK.getSelection(), 1, 0)).setDisplayName("§7Sortieren: §bRangJump").getStack());
        inventory.setItem(50, (new ItemCreator(Material.DOUBLE_PLANT, 1, 0)).setDisplayName("§7Sortieren: §6Gewinn").getStack());

        warps.clear();

        return inventory;
    }

    public void openNextPage(Inventory inventory, int page, Type type, int sortId) {
        List<String> warps = new ArrayList<>();

        putWarpsFromWeightInList(warps, type.getWeight());

        if (sortId == 1) {
            for (int i = 0; i <= warps.size() - 1; i++) {
                for (int i2 = 0; i2 <= warps.size() - 1; i2++) {
                    if (WarpsManager.getReward(warps.get(i)) > WarpsManager.getReward(warps.get(i2))) {
                        String temp = warps.get(i2);
                        warps.set(i2, warps.get(i));
                        warps.set(i, temp);
                    }
                }
            }
        }

        if (warps.isEmpty()) {
            inventory.setItem(52, new ItemStack(Material.AIR));
            inventory.setItem(53, new ItemStack(Material.AIR));
            for (int i = 0; i < 45; i++) {
                inventory.setItem(i, new ItemStack(Material.AIR));
            }

            return;
        }
        if (warps.size() <= 45) {
            int i;
            for (i = 0; i < 45; i++) {
                inventory.setItem(i, new ItemStack(Material.AIR));
            }
            for (i = 0; i < warps.size(); i++) {
                inventory.setItem(i, (new ItemCreator(type.getWarp(), 1, 0)).setDisplayName("§a" + warps.get(i))
                        .addLoreLine("§7Gewinn§8: §a" + WarpsManager.getReward(warps.get(i)) + " §7Mafis//§7Bestzeit§8: §a" +
                                getBest(warps.get(i)) + " §7Sekunden//§7Durchschnittszeit§8: §a" +
                                getAverage(warps.get(i)) + " §7Sekunden").getStack());
            }
            inventory.setItem(52, new ItemStack(Material.AIR));
            inventory.setItem(53, new ItemStack(Material.AIR));
            warps.clear();

            return;
        }
        if (warps.size() > 45 * page) {
            if (page == 1) {
                for (int i = 0; i < 45 * page; i++) {
                    inventory.setItem(i, (new ItemCreator(type.getWarp(), 1, 0)).setDisplayName("§a" + warps.get(i))
                            .addLoreLine("§7Gewinn§8: §a" + WarpsManager.getReward(warps.get(i)) + " §7Mafis//§7Bestzeit§8: §a" +
                                    getBest(warps.get(i)) + " §7Sekunden//§7Durchschnittszeit§8: §a" +
                                    getAverage(warps.get(i)) + " §7Sekunden").getStack());
                }
                inventory.setItem(52, new ItemStack(Material.AIR));
                inventory.setItem(53, (new ItemCreator(Material.MAP, 1, 0)).setDisplayName("§22 §7Seite").getStack());
            } else {
                List<String> list = warps.subList(45 * (page - 1), 45 * page);
                for (int i = 0; i < 45; i++) {
                    inventory.setItem(i, (new ItemCreator(type.getWarp(), 1, 0)).setDisplayName("§a" + list.get(i))
                            .addLoreLine("§7Gewinn§8: §a" + WarpsManager.getReward(list.get(i)) + " §7Mafis//§7Bestzeit§8: §a" +
                                    getBest(list.get(i)) + " §7Sekunden//§7Durchschnittszeit§8: §a" +
                                    getAverage(list.get(i)) + " §7Sekunden").getStack());
                }
                inventory.setItem(52, (new ItemCreator(Material.MAP, 1, 0)).setDisplayName("§2" + (page - 1) + " §7Seite").getStack());
                inventory.setItem(53, (new ItemCreator(Material.MAP, 1, 0)).setDisplayName("§2" + (page + 1) + " §7Seite").getStack());
                list.clear();
            }
            warps.clear();
        } else {
            List<String> list = warps.subList(45 * page - 45, warps.size());
            int i;
            for (i = 0; i < 45; i++) {
                inventory.setItem(i, new ItemStack(Material.AIR));
            }
            for (i = 0; i < list.size(); i++) {
                inventory.setItem(i, (new ItemCreator(type.getWarp(), 1, 0)).setDisplayName("§a" + list.get(i))
                        .addLoreLine("§7Gewinn§8: §a" + WarpsManager.getReward(list.get(i)) + " §7Mafis//§7Bestzeit§8: §a" +
                                getBest(list.get(i)) + " §7Sekunden//§7Durchschnittszeit§8: §a" +
                                getAverage(list.get(i)) + " §7Sekunden").getStack());
            }
            if (page == 1) {
                inventory.setItem(52, new ItemStack(Material.AIR));
                inventory.setItem(53, (new ItemCreator(Material.MAP, 1, 0)).setDisplayName("§2" + (page + 1) + " §7Seite").getStack());
            } else {
                inventory.setItem(52, (new ItemCreator(Material.MAP, 1, 0)).setDisplayName("§2" + (page - 1) + " §7Seite").getStack());
                inventory.setItem(53, new ItemStack(Material.AIR));
            }
            list.clear();
        }
        warps.clear();
    }

    private void putWarpsFromWeightInList(List<String> warps, int weight) {
        this.plugin.getMysqlConnection().query("SELECT warp FROM warp_list WHERE weight = ?", ps -> ps.setInt(1, weight), rs -> {
            warps.add(rs.getString("warp"));
            while (rs.next()) {
                warps.add(rs.getString("warp"));
            }
            return warps;
        });
    }

    private int getBest(String jump) {
        if (!MafiJumpPlugin.jumpcfg.contains("jumps." + jump + ".bestTime")) {
            return 0;
        }
        String time = MafiJumpPlugin.jumpcfg.getString("jumps." + jump + ".bestTime");
        return Integer.parseInt(time.split(",")[1]);
    }

    private int getAverage(String jump) {
        if (!MafiJumpPlugin.jumpcfg.contains("jumps." + jump + ".averageTime")) {
            return 0;
        }
        return MafiJumpPlugin.jumpcfg.getInt("jumps." + jump + ".averageTime");
    }

    public Map<UUID, Type> getType() {
        return this.type;
    }

    public enum Type {
        EASY(0, 45, Material.EMERALD, Material.EMERALD_BLOCK),
        NORMAL(1, 46, Material.GOLD_INGOT, Material.GOLD_BLOCK),
        HARD(2, 47, Material.IRON_INGOT, Material.IRON_BLOCK),
        HARDCORE(3, 48, Material.REDSTONE, Material.BEACON),
        RANK(4, 49, Material.DIAMOND, Material.DIAMOND_BLOCK);

        private final int weight;
        private final int slotID;
        private final Material warp;
        private final Material selection;

        Type(int weight, int slotID, Material warp, Material selection) {
            this.weight = weight;
            this.slotID = slotID;
            this.warp = warp;
            this.selection = selection;
        }

        public int getWeight() {
            return this.weight;
        }

        public int getSlotID() {
            return this.slotID;
        }

        public Material getWarp() {
            return this.warp;
        }

        public Material getSelection() {
            return this.selection;
        }
    }
}
