package de.maficraft.mafiport.commands;

import de.maficraft.mafiport.MafiPortPlugin;
import de.maficraft.mafiport.warp.WarpProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MafiPortCommand implements CommandExecutor {
    private final MafiPortPlugin plugin = MafiPortPlugin.getPlugin(MafiPortPlugin.class);

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof org.bukkit.command.ConsoleCommandSender) return true;

        Player player = (Player) sender;

        if (!player.hasPermission("mafiport.warp.setup")) return true;

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("remove")) {
                WarpProvider warp = new WarpProvider(args[1]);
                if (!warp.exists()) {
                    player.sendMessage(this.plugin.getPrefix() + "§cDer Warp §e" + args[1] + " §cexistiert nicht!");
                    return true;
                }
                warp.remove();
                player.sendMessage(this.plugin.getPrefix() + "Der Warp §e" + args[1] + " §fwurde gelöscht.");
                return true;

            } else if (args[0].equalsIgnoreCase("create")) {
                // create Warp ohne Schwierigkeitsgrad (weight)

                new WarpProvider((args[1])).create(player.getLocation());
                player.sendMessage(this.plugin.getPrefix() + "Warp §e" + args[1] + " §fohne Schwierigkeitsgrad erstellt.");
                return true;
            }

        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("create")) {
                // create Warp mit Schwierigkeitsgrad (weight)

                switch (args[2]) {
                    case "leicht":
                        (new WarpProvider(args[1])).create(player.getLocation(), 0);
                        player.sendMessage(this.plugin.getPrefix() + "Warp §e" + args[1] + " §fmit der Schwierigkeit §aLeicht §ferstellt.");
                        return true;

                    case "normal":
                        (new WarpProvider(args[1])).create(player.getLocation(), 1);
                        player.sendMessage(this.plugin.getPrefix() + "Warp §e" + args[1] + " §fmit der Schwierigkeit §eNormal §ferstellt.");
                        return true;

                    case "schwer":
                        (new WarpProvider(args[1])).create(player.getLocation(), 2);
                        player.sendMessage(this.plugin.getPrefix() + "Warp §e" + args[1] + " §fmit der Schwierigkeit §cSchwer §ferstellt.");
                        return true;

                    case "hardcore":
                        (new WarpProvider(args[1])).create(player.getLocation(), 3);
                        player.sendMessage(this.plugin.getPrefix() + "Warp §e" + args[1] + " §fmit der Schwierigkeit §4Hardcore §ferstellt.");
                        return true;

                    case "rangjump":
                        (new WarpProvider(args[1])).create(player.getLocation(), 4);
                        player.sendMessage(this.plugin.getPrefix() + "Warp §e" + args[1] + " §fmit der Schwierigkeit §bRangJump §ferstellt.");
                        return true;
                }

                player.sendMessage(this.plugin.getPrefix() + "Benutze §8/§fmp create <Name> [leicht,normal,schwer,hardcore,rangjump]");

            }

        } else {
            player.sendMessage(this.plugin.getPrefix() + "Benutze §8/§fmp create <Name> [leicht,normal,schwer,hardcore,rangjump]");
            player.sendMessage(this.plugin.getPrefix() + "Benutze §8/§fmp remove <Name>");
            return true;
        }

        return true;
    }
}
