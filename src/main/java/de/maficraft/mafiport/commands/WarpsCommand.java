package de.maficraft.mafiport.commands;

import de.maficraft.mafiport.MafiPortPlugin;
import de.maficraft.mafiport.warp.InventoryUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpsCommand implements CommandExecutor {
    private final MafiPortPlugin plugin = MafiPortPlugin.getPlugin(MafiPortPlugin.class);


    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof org.bukkit.command.ConsoleCommandSender) return true;

        Player player = (Player) sender;

        this.plugin.getInventoryUtil().getType().put(player.getUniqueId(), InventoryUtil.Type.EASY);

        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> player.openInventory(this.plugin.getInventoryUtil().warpInventory()));

        return true;
    }
}
