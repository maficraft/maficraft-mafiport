package de.maficraft.mafiport.commands;

import de.maficraft.mafiport.MafiPortPlugin;
import de.maficraft.mafiport.warp.WarpProvider;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WarpCommand implements CommandExecutor {
    private final MafiPortPlugin plugin = MafiPortPlugin.getPlugin(MafiPortPlugin.class);

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof org.bukkit.command.ConsoleCommandSender) return true;

        Player player = (Player) sender;

        if (args.length == 1) {

            WarpProvider warp = new WarpProvider(args[0]);

            if (!warp.exists()) {
                player.sendMessage(this.plugin.getPrefix() + "§cDer Warp §e" + args[0] + " §cexistiert nicht!");
                return true;
            }

            player.teleport(warp.getLocation());
            player.getActivePotionEffects().clear();
            player.setHealth(player.getMaxHealth());
            player.setFireTicks(0);
            player.sendMessage(this.plugin.getPrefix() + "Du wurdest zum Warp §e" + args[0] + " §fteleportiert!");
        } else {
            player.sendMessage(this.plugin.getPrefix() + "Benutze §8/§fwarp <Name>, oder §8/§fwarps.");
        }
        return true;
    }
}
