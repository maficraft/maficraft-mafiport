package de.maficraft.mafiport;

import de.maficraft.mafiport.commands.MafiPortCommand;
import de.maficraft.mafiport.commands.WarpCommand;
import de.maficraft.mafiport.commands.WarpsCommand;
import de.maficraft.mafiport.listener.InventoryListener;
import de.maficraft.mafiport.warp.InventoryUtil;
import me.xkuyax.utils.config.Config;
import me.xkuyax.utils.mysql.MysqlConnection;
import me.xkuyax.utils.mysql.MysqlConnectionUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.logging.Level;

public class MafiPortPlugin extends JavaPlugin {
    private MysqlConnection mysqlConnection;
    private InventoryUtil inventoryUtil;

    @Override
    public void onEnable() {
        try {
            this.mysqlConnection = MysqlConnectionUtils.hikari(new Config("./plugins/MafiPort/database.yml"), "mySqlConnection");
            this.mysqlConnection.prepare("CREATE TABLE IF NOT EXISTS warp_list (id_warp INT NOT NULL AUTO_INCREMENT, warp VARCHAR(36), world VARCHAR(36), CordX DOUBLE, CordY DOUBLE, CordZ DOUBLE, CordYaw FLOAT, CordPitch FLOAT, weight TINYINT(1), PRIMARY KEY(id_warp)) DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci");

        } catch (Exception e) {
            getLogger().log(Level.WARNING, "mysqlConnection: false");
        }

        this.inventoryUtil = new InventoryUtil();

        Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);

        getCommand("mp").setExecutor(new MafiPortCommand());
        getCommand("warp").setExecutor(new WarpCommand());
        getCommand("warps").setExecutor(new WarpsCommand());

        getLogger().log(Level.INFO, "Plugin activated!");
        getLogger().log(Level.INFO, "Version: " + getDescription().getVersion());
        getLogger().log(Level.INFO, "Plugin by " + getDescription().getAuthors());
    }


    @Override
    public void onDisable() {
        try {
            this.mysqlConnection.getDataSource().getConnection().close();
        } catch (SQLException e) {
            getLogger().log(Level.WARNING, "MySQL error: " + e.getMessage());
        }
        getLogger().log(Level.INFO, "Plugin deactivated!");
        getLogger().log(Level.INFO, "Version: " + getDescription().getVersion());
        getLogger().log(Level.INFO, "Plugin by " + getDescription().getAuthors());
    }

    public MysqlConnection getMysqlConnection() {
        return this.mysqlConnection;
    }

    public InventoryUtil getInventoryUtil() {
        return this.inventoryUtil;
    }

    public String getPrefix() {
        return "§8[§aM§7afiPort§8]§f ";
    }
}
