package de.maficraft.mafiport.listener;

import de.maficraft.mafiport.MafiPortPlugin;
import de.maficraft.mafiport.warp.InventoryUtil;
import de.maficraft.mafiport.warp.ItemCreator;
import de.maficraft.mafiport.warp.WarpProvider;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {
    private final MafiPortPlugin plugin = MafiPortPlugin.getPlugin(MafiPortPlugin.class);

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) return;
        if (event.getClickedInventory() == null) return;
        if (event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR)) return;
        if (event.getClickedInventory().getName() == null) return;

        Player player = (Player) event.getWhoClicked();
        if (event.getClickedInventory().getName().equalsIgnoreCase(this.plugin.getInventoryUtil().warpInventory().getName())) {
            event.setCancelled(true);
            for (InventoryUtil.Type type : InventoryUtil.Type.values()) {
                if (event.getCurrentItem().getType().equals(type.getSelection())) {
                    if (!event.getCurrentItem().containsEnchantment(Enchantment.ARROW_DAMAGE)) {
                        for (int i = InventoryUtil.Type.EASY.getSlotID(); i < InventoryUtil.Type.RANK.getSlotID() + 1; i++) {
                            event.getClickedInventory().getItem(i).removeEnchantment(Enchantment.ARROW_DAMAGE);
                        }

                        ItemStack stack = (new ItemCreator(event.getCurrentItem().getType(), 1, 0)).setDisplayName(event.getCurrentItem().getItemMeta().getDisplayName()).setEnchanted(true).getStack();
                        event.getClickedInventory().setItem(type.getSlotID(), stack);
                        this.plugin.getInventoryUtil().getType().put(player.getUniqueId(), type);
                        if (event.getClickedInventory().getItem(50).containsEnchantment(Enchantment.ARROW_DAMAGE)) {
                            Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> this.plugin.getInventoryUtil().openNextPage(event.getClickedInventory(), 1, type, 1));
                        } else {
                            Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> this.plugin.getInventoryUtil().openNextPage(event.getClickedInventory(), 1, type, 0));
                        }
                        return;
                    }
                } else {
                    if (event.getCurrentItem().getType().equals(type.getWarp())) {
                        String name = event.getCurrentItem().getItemMeta().getDisplayName().replaceAll("§((?i)[0-9a-fk-or])", "");
                        WarpProvider warp = new WarpProvider(name);
                        player.teleport(warp.getLocation());
                        player.sendMessage(this.plugin.getPrefix() + "Du wurdest zum Warp §e" + name + " §fteleportiert!");
                        return;
                    }
                    if (event.getCurrentItem().getType().equals(Material.DOUBLE_PLANT) &&
                            event.getClickedInventory().getItem(0).getType().equals(type.getWarp())) {
                        ItemStack currentItem;
                        for (int i = InventoryUtil.Type.EASY.getSlotID(); i < InventoryUtil.Type.RANK.getSlotID() + 1; i++) {
                            event.getClickedInventory().getItem(i).removeEnchantment(Enchantment.ARROW_DAMAGE);
                        }

                        ItemStack stack = (new ItemCreator(type.getSelection(), 1, 0)).setDisplayName(event.getClickedInventory().getItem(type.getSlotID()).getItemMeta().getDisplayName()).setEnchanted(true).getStack();

                        if (event.getCurrentItem().containsEnchantment(Enchantment.ARROW_DAMAGE)) {

                            currentItem = (new ItemCreator(event.getCurrentItem().getType(), 1, 0)).setDisplayName(event.getCurrentItem().getItemMeta().getDisplayName()).setEnchanted(false).getStack();
                            Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> this.plugin.getInventoryUtil().openNextPage(event.getClickedInventory(), 1, type, 0));
                        } else {

                            currentItem = (new ItemCreator(event.getCurrentItem().getType(), 1, 0)).setDisplayName(event.getCurrentItem().getItemMeta().getDisplayName()).setEnchanted(true).getStack();
                            Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> this.plugin.getInventoryUtil().openNextPage(event.getClickedInventory(), 1, type, 1));
                        }
                        event.getClickedInventory().setItem(event.getSlot(), currentItem);
                        event.getClickedInventory().setItem(type.getSlotID(), stack);

                        return;
                    }
                }
            }
            if (event.getCurrentItem().getType().equals(Material.MAP)) {
                int page = Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName()
                        .replaceAll("§((?i)[0-9a-fk-or])", "").replaceAll(" Seite", ""));
                if (event.getClickedInventory().getItem(50).containsEnchantment(Enchantment.ARROW_DAMAGE)) {
                    Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> this.plugin.getInventoryUtil().openNextPage(event.getClickedInventory(), page, this.plugin.getInventoryUtil().getType().get(player.getUniqueId()), 1));
                } else {
                    Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> this.plugin.getInventoryUtil().openNextPage(event.getClickedInventory(), page, this.plugin.getInventoryUtil().getType().get(player.getUniqueId()), 0));
                }
            }
        }
    }


    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        if (event.getInventory().getName() == null)
            return;
        if (event.getInventory().getName().equalsIgnoreCase(this.plugin.getInventoryUtil().warpInventory().getName()))
            this.plugin.getInventoryUtil().getType().remove(player.getUniqueId());
    }
}
